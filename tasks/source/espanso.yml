# Install and update [Espanso](https://espanso.org/) the cool text expander !

- name: Clone or pull changes from Espanso repository
  register: espanso_repo_status
  ansible.builtin.git:
    repo: https://github.com/federico-terzi/espanso
    dest: "{{ compilation_from_source_folder }}/espanso"

# Based on https://espanso.org/docs/install/linux/#wayland-compile
- name: Install build dependencies for Espanso
  become: true
  ansible.builtin.dnf:
    state: present
    name:
      - "@development-tools"
      - gcc-c++
      - wl-clipboard
      - libxkbcommon-devel
      - dbus-devel
      - wxGTK-devel.x86_64
      - openssl
      - openssl-devel

- name: "[async] Build Espanso from source and install it the first time"
  register: async_espanso
  poll: 0 # async as it is slow and doesn't conflict with another task
  async: 1000
  shell: |
    cargo make --profile release --env NO_X11=true build-binary
    mv target/release/espanso ~/.cargo/bin/espanso
  args:
    chdir: "{{ compilation_from_source_folder }}/espanso"
    creates: ~/.cargo/bin/espanso

- name: Register local systemd service
  # TODO: really useful to change PATH ?
  environment: 
    PATH: "{{ ansible_env.HOME }}/.cargo/bin:{{ ansible_env.PATH }}"
  command:
    cmd: "espanso service register"
    creates: "{{ ansible_env.HOME }}/.config/systemd/user/espanso.service"

# Espanso actually needs to access some input interfaces to detect triggers and inject expansions. See more in docs about security aspects.
# From: https://espanso.org/docs/install/linux/#adding-the-required-capabilities
# This corresponds to running "sudo setcap "cap_dac_override+p" $(which espanso)"
- name: Enable access to input devices
  become: true
  community.general.capabilities:
      path: "{{ ansible_env.HOME }}/.cargo/bin/espanso"
      capability: cap_dac_override+p
      state: present
    
# Note: git pull was made by above task using git module
# TODO: is it okay to manage updates like this ?
- name: "Update Espanso if there are new commits"
  when: espanso_repo_status.after != espanso_repo_status.before
  shell: |
    cargo make --profile release --env NO_X11=true build-binary
    mv target/release/espanso ~/.cargo/bin/espanso
  args:
    chdir: "{{ compilation_from_source_folder }}/espanso"
