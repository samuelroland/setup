This folder contain scripts proposed only to be run with `curl https://...../script.sh | bash`, that I first download, try to quickly review and store here to avoid script dynamically changing to running malicious commands on my system after a server hack. It doesn't protect us from all security threats obviously, it's just a little better. I will update those scripts from time to time if they change.

I do not change the content of these scripts, the license of this repository does not apply to this folder ! See respective licence generally indicated at the top of the scripts.
Read more on 
1. https://security.stackexchange.com/questions/213401/is-curl-something-sudo-bash-a-reasonably-safe-installation-method or 

## Scripts list
This list of URLS and files can be deduced from below script.

Update script to download all scripts, review them before commiting them.
```sh
curl -s "https://get.sdkman.io"  > sdkman.sh
```
